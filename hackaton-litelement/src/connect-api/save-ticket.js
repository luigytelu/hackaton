import { LitElement, html } from 'lit-element';
class SaveTicket extends LitElement {
    static get properties() {
        return {
            branches: {type: Array},
            tsec: {type: String}
        };
    }
    constructor() {
        super();
        
    }

    saveTicket(e) {
        console.log("getToken");
        console.log("Obteniendo Token");
        let xhrGT = new XMLHttpRequest();
        xhrGT.onload = () => {
            if (xhrGT.status === 200) {
                console.log("getToken - Petición completada correctamente");
                this.tsec = xhrGT.getResponseHeader("tsec");
                
                let xhr = new XMLHttpRequest();
                xhr.onload = () => {
                    if (xhr.status === 200) {
                        console.log("getBranches - Petición completada correctamente");
                        let APIResponse = JSON.parse(xhr.responseText);
                        this.branches = APIResponse.data;
                        this.dispatchEvent(
                            new CustomEvent("obtener-listado-oficinas", {
                                "detail":{
                                    data : this.branches
                                }})
                        )
                    }
                }
                xhr.open("GET", "https://cal.glomo.bbvacontinental.pe/SRVS_A02/pois/v0/bank-pois?latitude=-12.010483&longitude=-77.060256&type.id=ATM");
                xhr.setRequestHeader("tsec", this.tsec);
                xhr.send();
            }
        }
        xhrGT.open("POST", "https://cal.glomo.bbvacontinental.pe/SRVS_A02/TechArchitecture/pe/grantingTicket/V02");
        xhrGT.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhrGT.send(JSON.stringify({"authentication":{"userID":"13000014","consumerID":"13000014","authenticationType":"61","authenticationData":[]},"backendUserRequest":{"userId":"","accessCode":"","dialogId":""}}));
    }
}
customElements.define('save-ticket', SaveTicket)
