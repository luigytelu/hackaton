import { LitElement, html } from 'lit-element';
class GetBranches extends LitElement {
    static get properties() {
        return {
            branches: {type: Array},
            currentLatitude: {type: String},
            currentLongitude: {type: String},
            initialLatitudeGet: {type: String},
            initialLongitudeGet: {type: String}
        };
    }
    constructor() {
        super();
        this.branches = [];
        this.getBranches();
        this.currentLatitude = "";
        this.currentLongitude = "";
    }

    updateNetLocation(){
        console.log("getBranches");
        this.getBranches();
    }

    getBranches(e) {
        console.log("getBranches");
        this.initialLatitudeGet = "-12.232321325580973";//"-12.007598";
        this.initialLongitudeGet = "-76.90507618031445";//"-77.060521";

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("getBranches - Petición completada correctamente");
                console.log("Obteniendo Oficinas");

                let APIResponse = JSON.parse(xhr.responseText);
                this.branches = APIResponse.data;
                this.dispatchEvent(
                    new CustomEvent("obtener-listado-oficinas", {
                        "detail":{
                            data : this.branches
                        }})
                )
            }
        }
        let url = "";
        if (this.currentLatitude) {
            url = "http://localhost:8000/getPois.json?latitude="+this.currentLatitude+"&longitude="+this.currentLongitude;
        } else {
            url = "http://localhost:8000/getPois.json?latitude="+this.initialLatitudeGet+"&longitude="+this.initialLongitudeGet;
        }

        //let url = "http://localhost:8000/getPois.json?latitude="+this.currentLatitude+"&longitude="+this.currentLongitude;
        console.log(url);
        xhr.open("GET", url);
        xhr.send();
    }
}
customElements.define('get-branches', GetBranches)
