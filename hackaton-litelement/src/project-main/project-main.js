import { LitElement, html, css } from 'lit-element';
import "../project-header/project-header.js";
import "../project-container/project-container.js";
import "../project-footer/project-footer.js";
export class ProjectMain extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <project-header></project-header>
      <project-container></project-container>
      <project-footer></project-footer>

    `;
  }
}

customElements.define('project-main', ProjectMain)
