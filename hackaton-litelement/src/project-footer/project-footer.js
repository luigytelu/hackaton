import { LitElement, html, css } from 'lit-element';

export class ProjectFooter extends LitElement {
  static get properties() {
    return {
    };
  }
  constructor() {
    super();
  }
  static get styles() {
    return css`
      .footer{
        height: 70px;
      }
    `;
  }

  render() {
    return html`
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <footer class="footer"  style="background-color: #004481">
        <div class="container">
            <div class="row ">
                <div class="col-md-12 text-white my-4 text-center text-md-left ">
                    <span class=" font-weight-ligth ">Equipo 1 Hackathon Practicioner</span>
                </div>
            </div>
        </div>
     </footer>
    `;
  }
}

customElements.define('project-footer', ProjectFooter)
