import { LitElement, html, css } from 'lit-element';
import "../project-navbar/project-navbar.js";
import "../sucursal-item/sucursal-item.js";
import "../connect-api/get-branches.js";
import "../project-container/mostrar-oficinas.js";
import "../project-container/crear-ticket.js";
export class ProjectContainer extends LitElement {
  static get properties() {
    return {
        branches: {type: Array},
        branchId : {type: String},
        branchName : {type: String},
        branchAddress : {type: String}
    };
  }
  constructor() {
    super();
    this.branches = [];
    this.branchId = "";
    this.branchName = "";
    this.branchAddress = "";
  }
  static get styles() {
    return css`
        .wrap {
            min-height: 100%;
            height: auto !important;
            height: 100%;
            /* Negative indent footer by it's height */
            margin: 0 auto -30px;
        }
        .container{
            overflow:auto;
            padding-bottom:150px;
        }
        .push{
            height: 70px;
        }
    `;
  }

  render() {
    return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <div class="wrap">
            <project-navbar  @go-startPage="${this.goToStartPage}"></project-navbar>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ">
                        <mostrar-oficinas id="idViewOffice" @go-ticketPage="${this.goToTicketPage}"></mostrar-oficinas>
                        <crear-ticket id="idCreateTicket" branchId="${this.branchId}" branchName="${this.branchName}" branchAddress="${this.branchAddress}" @new-person="${this.newShowOffice}" @go-startPage="${this.goToStartPage}" class="d-none"></crear-ticket>
                    </div>
                </div>
            </div>
            <div class="push"></div>
        </div>
    `;
  }

  goToStartPage() {
      console.log("showOffice");
      this.shadowRoot.getElementById("idViewOffice").classList.remove("d-none");
      this.shadowRoot.getElementById("idCreateTicket").classList.add("d-none");
  }

  goToTicketPage(e) {
      this.branchId = e.detail.branchId;
      this.branchName = e.detail.branchName;
      this.branchAddress = e.detail.branchAddress;
      this.shadowRoot.querySelector("project-navbar").shadowRoot.getElementById("itemStarPage").classList.remove("active");
      this.shadowRoot.querySelector("project-navbar").shadowRoot.getElementById("itemTicketPage").classList.add("active");
      this.shadowRoot.getElementById("idViewOffice").classList.add("d-none");
      this.shadowRoot.getElementById("idCreateTicket").classList.remove("d-none");
  }
}
customElements.define('project-container', ProjectContainer)
