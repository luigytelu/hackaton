import { LitElement, html, css } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';
export class ShowModal extends LitElement {
    constructor () {
        super()
        this.opened = false
        console.log('Dialog visible:', this.opened)
    }
    
    static get properties () {
        return {
          opened: {type: Boolean}
        }
    }
    static get styles() {
        return css`
        .opened {
            display: flex;
            transform: scale(1);
          transition: visibility 0s linear 0s,opacity .25s 0s,transform .25s;
        }
        .closed {
            display: none;
        }
        .dialog {
            flex-direction: column;
            border: 2px outset black;
            padding: 1em;
            margin: 1em;
        }
        .buttons {
            display: flex;
            flex-direction: row;
        }
        .accept {
            justify-content: space-around;
            align-content: space-around;
        }
        .cancel {
            justify-content: space-around;
            align-content: space-around;
        }
        `;
    }
    
    render () {
        return html`
        <div class="${classMap({dialog: true, opened: !this.opened, closed: this.opened})}">
            <h1 class="title ">Title</h1>
            <p class="content">This is a dialog</p>
            <div class="buttons">
            <button class="accept" @click="${() => this.dispatchEvent(new CustomEvent('dialog.accept'))}">Ok</button>
            <button class="cancel" @click="${() => this.dispatchEvent(new CustomEvent('dialog.cancel'))}">Cancel</button>    
            </div>
        </div>`
      }
    }

customElements.define('show-modal', ShowModal)
