import { LitElement, html, css } from 'lit-element';
import '../../node_modules/@doubletrade/lit-dialog/lit-dialog.js';
class CrearTicket extends LitElement {

    static get properties() {
        return {
            branchId: {type: String},
            branchName: {type: String},
            branchAddress: {type: String},
            ticket: {type: Object},
            selectedTypeDocument: {type: String},
            selectednumberDocument: {type: String},
            selectedIsPreferential: {type: String},
            createTickerRequest: {type: String},
            responseMessage: {type: String},
            status: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.branchId = "";
        this.branchName = "";
        this.branchAddress = "";
        this.ticket = {};
        this.selectedTypeDocument = "";
        this.selectednumberDocument = "";
        this.selectedIsPreferential = "";
        this.responseMessage = "";
        this.status = false;
    }

    static get styles() {
        return css`
          .heading{
            background: #004481;
            margin-bottom: 0;
            padding: 10px;
            font-size: 20px;
            border-radius: 3px 3px 0 0;
            color: white;
            margin-top: 50px !important;
            color: white;
            text-align: center;
          }
          .white{
              back
          }
        `;
      }

    render() {
        return html `
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <div class="col-md-12 order-md-1 ">
                <h3 class="heading" @click="${this.createTicket}">Crear Ticket</h3>
            </div>
            <div class="col-md-12">
                <br/>
                <div class="row">
                            <div class="col-md-12">
                                <div class="card flex-md-row mb-12 box-shadow ">
                                    <div class="card-body d-flex flex-column align-items-start">
                                    <h6 class="mb-0">
                                        Sucursal:
                                        <strong> ${this.branchName} </strong>
                                    </h6>
                                    <div class="mb-1 text-muted">${this.branchAddress}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <br/>
                <form>
                    <div class="form-group">
                        <label>Tipo Documento</label>
                        <div class="dropdown show">
                            <select id="idSelTypeDoc" name="select" class="form-control" @input="${this.updateTypeDocument}">
                              <option value="L">DNI</option>
                              <option value="R">RUC</option>
                              <option value="P">Pasaporte</option>
                              <option value="E">Carnet de Extranjería</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Número Documento</label>
                        <input @input="${this.updateNumberDocument}" type="text" maxlength="8" id="ticketFormNumDoc" class="form-control" placeholder="Ingrese su número de documento"/>
                    </div>
                    <div class="form-group">
                        <label>Preferencial</label>
                        <div class="dropdown show">
                            <select id="idSelIsPref" name="select" class="form-control" @input="${this.updateIsPreferential}">
                              <option value="S">Si</option>
                              <option value="N" selected>No</option>
                            </select>
                        </div>
                    </div>
                    <button @click="${this.goToStartPage}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.sendSaveTicket}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
            <lit-dialog  title="${this.responseMessage}" closeIcon closeOnEsc closeOnClickOutside @opened-changed="${this.onCloseModal}">
            </lit-dialog>

        `;
    }

    goToStartPage(e) {
        e.preventDefault();
        console.log("goBack");
        this.dispatchEvent(new CustomEvent("go-startPage", {}));
    }

    updateTypeDocument(e) {
        this.ticket.typeDocument = e.target.value;
    }

    updateNumberDocument(e) {
        this.ticket.numberDocument = e.target.value;
    }

    updateIsPreferential(e) {
        this.ticket.isPreferential = e.target.value;
    }

    sendSaveTicket(e) {
        e.preventDefault();
        console.log("storeTicket");

        this.selectedTypeDocument = this.shadowRoot.querySelector('#idSelTypeDoc').value;
        this.selectednumberDocument = this.ticket.numberDocument;
        this.selectedIsPreferential = this.shadowRoot.querySelector('#idSelIsPref').value;

        if(this.selectednumberDocument === '' || this.selectednumberDocument ===undefined){
            console.log("empty numberofdocument");
            this.responseMessage = "Ingrese un número de documento" ;
            this.shadowRoot.querySelector("lit-dialog").open();
        }else{
            this.createNewTicket();
        }
    }
    onCloseModal(e){
        if(!e.detail.value && this.status){
            location.reload();
        }
    }

    createNewTicket() {
        let xhr = new XMLHttpRequest();
        xhr.onload = () => {

            if (xhr.status === 201) {
                console.log("createTicket - Petición completada correctamente");
                this.responseMessage = "Ticket Generado" ;
                this.status = true;
            } else {
                this.responseMessage = "Lo sentimos, no hemos podido generar tu ticket en este momento" ;
            }
            this.shadowRoot.querySelector("lit-dialog").open();
        }
        xhr.open("POST", "https://cal.glomo.bbvacontinental.pe/SRVS_A02/TechArchitecture/pe/grantingTicket/V02");
        xhr.setRequestHeader("Content-Type", "application/json");
        this.createTickerRequest = JSON.stringify({"idOffice":this.branchId,"typeDocument":this.selectedTypeDocument,"numberDocument":this.selectednumberDocument,"isPreferential":this.selectedIsPreferential});
        console.log("createTicket => " + this.createTickerRequest);
        xhr.send(this.createTickerRequest);
    }
}

customElements.define('crear-ticket', CrearTicket)

