import{ LitElement, html, css} from 'lit-element';
import "../connect-api/get-branches.js";
import "../sucursal-item/sucursal-item.js";
import "../project-container/mostrar-mapa.js";
class MostrarOficinas extends LitElement{
    static get properties() {
        return {
            branches: {type: Array},
            initialLatitude : {type: String},
            initialLongitude : {type: String}
        };
    }
    constructor() {
        super();
        this.branches = [];
        this.initialLatitude = "-12.232321325580973";//"-12.007598";
        this.initialLongitude = "-76.90507618031445";//"-77.060521";
    }

    static get styles() {
        return css`
          .heading{
            background: #004481;
            margin-bottom: 0;
            padding: 10px;
            font-size: 20px;
            border-radius: 3px 3px 0 0;
            color: white;
            margin-top: 50px !important;
            color: white;
            text-align: center;
          }
          .map{
            display: block;
            height: 600px;
          }
          .leftblock{
            padding-right: 0px;
            padding-left: 0px;
          }
          .rightblock{
            padding-right: 0px;
            padding-left: 0px;
          }
          .rounded-circle{
            width: 10px;
            height: 10px;
            margin-left: 0px;
          }
          .rect-circle{
            width: 100%;
            height: 100%;
          }
          .free{
            fill: #28a745;
          }
          .full{
            fill: #dc3545;
          }
          .intermediate{
            fill: #FF9033;
          }
        `;
      }

    render(){
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <div class="row">
              <h3 class=" col-md-12 heading" @click="${this.viewOffice}">Nuestras Oficinas</h3>
            </div>
            <div class="row">
              <div class="col-md-8 leftblock">
                <div class="map">
                  <mostrar-mapa id="mapReceiver" @update-new-location="${this.processEventNewLocation}"
                  initialLatitude="${this.initialLatitude}"
                  initialLongitude="${this.initialLongitude}"
                  ></mostrar-mapa>
                </div>
              </div>
              <div class="col-md-4 rightblock">
                  <ul class="list-group">
                    <li class="list-group-item">
                          <h6 class="pb-2 mb-0">Disponibilidad de Aforo</h6>
                          <div>
                            <svg class="rounded-circle"><rect class="rect-circle full"></rect></svg>
                            <small class="text-muted">Lleno</small>
                          </div>
                          <div>
                            <svg class="rounded-circle"><rect class="rect-circle intermediate"></rect></svg>
                            <small class="text-muted">Intermedio</small>
                          </div>
                          <div>
                            <svg class="rounded-circle"><rect class="rect-circle free"></rect></svg>
                            <small class="text-muted">Disponible</small>
                          </div>
                    </li>
                    <get-branches id="idGetBranches" @obtener-listado-oficinas="${this.processEvent}"
                    initialLatitudeGet="${this.initialLatitude}"
                    initialLongitudeGet="${this.initialLongitude}"
                    ></get-branches>
                    ${this.branches.map(
                      branch => html`
                        <sucursal-item @go-ticketPage="${this.goToTicketPage}"
                          branchId="${branch.id}" branchName="${branch.description}" branchAddress="${branch.address.addressName}"
                          branchState="${branch.isOpen}" branchCapacity="${branch.capacity.status}" branchCapacityTotal="${branch.capacity.totalCapacity}" branchCapacityActual="${branch.capacity.currentCapacity}">
                        </sucursal-item>
                      `
                    )}
                  </ul>
              </div>
            </div>
        `;
    }

    processEvent(e){
        console.log("Se ha capturado el evento del emisor processEvent");
        console.log(e);
        this.branches = e.detail.data;
        this.shadowRoot.getElementById("mapReceiver").branches = e.detail.data;
    }

    processEventNewLocation(e){
        console.log("Se ha capturado el evento del emisor processEventNewLocation");
        console.log(e);

        this.shadowRoot.getElementById("idGetBranches").currentLatitude = e.detail.currentLatitude;
        this.shadowRoot.getElementById("idGetBranches").currentLongitude = e.detail.currentLongitude;

        this.shadowRoot.querySelector("get-branches").updateNetLocation();
    }

    goToTicketPage(e){
      console.log("mostrar-oficinas"+ e.detail.branchId);
      console.log(Object.values(this.branches));

      let indexOfBranch = this.branches.findIndex(
          branch => branch.id === e.detail.branchId
      );

      if (indexOfBranch >= 0) {
          console.log("Branch encontrado");
          let chosenBranch = this.branches[indexOfBranch];
          this.dispatchEvent(new CustomEvent("go-ticketPage", {
            detail:{
              branchId: chosenBranch.id,
              branchName: chosenBranch.description,
              branchAddress: chosenBranch.address.addressName
            }
          }));
      }
    }
}

customElements.define('mostrar-oficinas', MostrarOficinas);
