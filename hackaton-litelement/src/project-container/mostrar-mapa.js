import{ LitElement, html,css } from 'lit-element';
import "../distGoogleMap/lit-google-map.bundle.js";
class MostrarMapa extends LitElement{
    static get properties() {
        return {
            branches: {type: Array},
            initialLatitude : {type: String},
            initialLongitude : {type: String}
        };
    }
    constructor() {
        super();
    }

    render(){
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <lit-google-map
                api-key="AIzaSyDLUdFsB0aDi9paOaigBBl6YbByijjY3sM"
                version="3.43" zoom="13"
                center-latitude="${this.initialLatitude}"
                center-longitude="${this.initialLongitude}"
                @update-location="${this.processEvent}"
                >
                ${this.branches.map(
                    branch => html`
                    <lit-google-map-marker slot="markers" latitude="${branch.address.geolocation.latitude}" longitude="${branch.address.geolocation.longitude}">
                    <p><span style="text-align: center"><strong>Sucursal: </strong>${branch.description}</span></p>
                    </lit-google-map-marker>
                    `
                )}
            </lit-google-map>
        `;
    }

    processEvent(e) {
        console.log("Se ha capturado el evento de google map");
        console.log(e.detail);
        this.dispatchEvent(new CustomEvent("update-new-location", {
          detail:{
              currentLatitude : e.detail.currentLatitude,
              currentLongitude : e.detail.currentLongitude
          }
        }));
    }
}

customElements.define('mostrar-mapa', MostrarMapa);
