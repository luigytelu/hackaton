import { LitElement, html, css } from 'lit-element';
export class SucursalItem extends LitElement {
  static get properties() {
    return {
        branchId: {type: String},
        branchName: {type: String},
        branchAddress: {type: String},
        branchState: {type: String},
        branchCapacity: {type: String},
        branchCapacityTotal: {type: String},
        branchCapacityActual: {type: String}
        
    };
  }
  constructor() {
    super();
    this.branchId = "";
    this.branchName = "";
    this.branchAddress = "";
    this.branchState = "";
    this.branchCapacity = "";
    this.branchCapacityTotal = "";
    this.branchCapacityActual = "";
    
  }
  static get styles() {
    return css`
      .rounded-circle{
        width: 10px;
        height: 10px;
        margin-left: 0px;
      }
      .rect-circle{
        width: 100%;
        height: 100%;
      }
      .bg-medium{
        background: #fff8eb;
      }
      .bg-free{
        background: #eaf8e3;
      }
      .full{
        fill: #dc3545;
      }
      .free{
        fill: #28a745;
      }
      .intermediate{
        fill: #FF9033;
      }
    `;
  }

  render() {
    console.log(this.branchState);
    return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        
        <li class="list-group-item d-flex justify-content-between ${this.branchState === "true" ? this.branchCapacity === 'S' ? 'bg-free' : this.branchCapacity === 'M'  ? 'bg-medium': 'lh-condensed' : ''}">
              <div class=" w-90 ">
                <h6 class="my-0">Of. ${this.branchName}</h6>
                ${this.branchState === "true" ? 
                 html `<p><small class="text-muted"><strong>Aforo: </strong>${this.branchCapacityActual}/${this.branchCapacityTotal}</small></p>` : 
                 html `<small class="text-danger"><strong>Oficina cerrada</small>`
                 }
                
                <p><small class="text-muted"><strong>Dirección: </strong>${this.branchAddress}</small></p>
                ${this.branchState === "true" ? 
                 html `<button @click="${this.goToAddTickePage}" type="button" class="btn btn-success btn-sm "><strong> + </strong> Ticket</button>` : 
                 html ``
                 }
                
              </div>
              <div class="text-muted w-10">
                <svg class="rounded-circle"><rect class="rect-circle ${this.branchCapacity === 'S' ? 'free' : this.branchCapacity === 'M'  ? 'intermediate': 'full'}"></rect></svg>
              </div>
        </li>

    `;
  }

  goToAddTickePage(e){
    this.dispatchEvent(
      new CustomEvent (
          "go-ticketPage", {
              "detail":{
                branchId : this.branchId
              }
          }
      )
    );
    
  }
}

customElements.define('sucursal-item', SucursalItem)
