import { LitElement, html, css } from 'lit-element';

export class ProjectNavbar extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  static get styles() {
    return css`
        .navbar{
            background-color: #004481;
        }
    `;
  }

  render() {
    return html`
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <nav class="navbar navbar-expand-lg navbar-dark " >
        <a class="navbar-brand">BBVA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
            <a id="itemStarPage" class="nav-item nav-link active" @click="${this.goToStartPage}">Nuestras Oficinas</a>
            <a id="itemTicketPage" class="nav-item nav-link">Crear ticket</a>
            </div>
        </div>
    </nav>
    `;
  }

  goToStartPage(){
    this.shadowRoot.getElementById("itemStarPage").classList.add("active");
    this.shadowRoot.getElementById("itemTicketPage").classList.remove("active");
    this.dispatchEvent(new CustomEvent("go-startPage", {}));
  }
}

customElements.define('project-navbar', ProjectNavbar)
